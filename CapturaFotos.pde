  

import processing.video.*;

Capture cam;

void setup() {
  size(1280, 720);

  String[] cameras = Capture.list();
  
  if (cameras.length == 0) {
    println("No hay cámaras disponibles para la captura.");
    exit();
  } else {
    println("Cámaras disponibles:");
    for (int i = 0; i < cameras.length; i++) {
      println(cameras[i]);
    }
    
    // La cámara puede ser inicializada directamente usando un 
    // elemnto del arreglo que regresa list():
    cam = new Capture(this, cameras[0]);
    cam.start();     
  }      
}

void draw() {
  if (cam.available() == true) {
    cam.read();
  }
  image(cam, 0, 0);
  // Lo siguiente hace lo mismo, y es rápido cuando dibuja la imagen
  // sin agregar redimensionamiento, transformaciones, or coloraciones.
  //set(0, 0, cam);
}
   // aquí guardamos una foto de la cámara
void mousePressed() {
   image(cam, 0, 0);
   saveFrame("data/foto.jpg");
   delay(200);
   cam.stop();
  
   image(cam,0,0);

       cam.start();
  

  
} 